#!/bin/bash
#
#from: http://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself

Does not use -f option in readlink, therefore should work in bsd/mac-osx
Supports

    source ./script (When called by the . dot operator)
    Absolute path /path/to/script
    Relative path like ./script
    /path/dir1/../dir2/dir3/../script
    When called from symlink
    When symlink is nested eg) foo->dir1/dir2/bar bar->./../doe doe->script
    When caller changes the scripts name

I am looking for corner cases where this code does not work. Please let me know.
Code

pushd . > /dev/null
SCRIPT_PATH="${BASH_SOURCE[0]}";
while([ -h "${SCRIPT_PATH}" ]); do
    cd "`dirname "${SCRIPT_PATH}"`"
    SCRIPT_PATH="$(readlink "`basename "${SCRIPT_PATH}"`")";
done
cd "`dirname "${SCRIPT_PATH}"`" > /dev/null
SCRIPT_PATH="`pwd`";
popd  > /dev/null
echo "srcipt=[${SCRIPT_PATH}]"
echo "pwd   =[`pwd`]"

Known issuse

Script must be on disk somewhere, let it be over a network. If you try to run this script from a PIPE it will not work