#!/bin/bash

url=git://github.com/some-user/my-repo.git

basename=$(basename $url)
echo "BASENAME: $basename"

filename=${basename%.*}
echo "FILENAME: $filename"

extension=${basename##*.}
echo "EXTENSION $extension"