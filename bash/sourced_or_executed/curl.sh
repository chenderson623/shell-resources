#!/bin/bash

curl -s https://bitbucket.org/chenderson623/shell-resources/raw/master/bash/sourced_or_executed/01.sh | bash -s

# OR:
bash <(curl -s https://bitbucket.org/chenderson623/shell-resources/raw/master/bash/sourced_or_executed/01.sh)
source <(curl -s https://bitbucket.org/chenderson623/shell-resources/raw/master/bash/sourced_or_executed/01.sh)

# OR:
wget -O - https://bitbucket.org/chenderson623/shell-resources/raw/master/bash/sourced_or_executed/01.sh | bash