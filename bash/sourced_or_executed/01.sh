#!/bin/bash

THISDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

[ -t 0 ] && echo "bash is reading a script from somewhere else" || echo "bash is reading a script from stdin"

sourced=0
echo "TEST SOURCED:"
echo "BASH SOURCE: ${BASH_SOURCE[0]}"
echo "0: ${0}"
echo "THISDIR: ${THISDIR}"
[[ "${BASH_SOURCE[0]}" != "${0}" ]] && sourced=1


if [ $sourced -eq 0 ];then
    echo "Directly Executed"
fi

if [ $sourced -eq 1 ];then
    echo "Sourced"
fi