# from: http://unix.stackexchange.com/questions/28283/autocomplete-of-filename-in-directory

_codeComplete()
{
    local cur=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "$(ls /something/)" -- $cur) )
}

complete -F _codeComplete hi